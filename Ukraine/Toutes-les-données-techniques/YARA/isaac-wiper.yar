rule IsaacWiper : malware {
    meta:
        description = "IsaacWiper"
        source = "OCD"
        date = "08/03/22"
        category = "wiper"
    strings:
        $ = "getting drives..." wide fullword
        $ = "C:\\ProgramData\\log.txt" wide fullword
        $ = "physical drives:" wide fullword
        $ = "-- system physical drive " wide fullword
        $ = "-- start erasing logical drive " wide fullword
        $ = "-- logical drive: " wide fullword
        $ = "-- system logical drive: " wide fullword
        $ = "PhysicalDrive" wide fullword
    condition:
        all of them
}
