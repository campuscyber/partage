rule DoubleZero : malware {
    meta:
        description = "DoubleZero Destructive Malware Used in Cyber-Attacks at Ukrainian Companies"
        source = "OCD"
        date = "31/03/22"
        category = "wiper"
        static = "false"
    strings:
        $ = "\\Users\\\\.*?\\\\Local Settings.*" wide fullword
        $ = "\\Users\\\\.*?\\\\AppData\\\\Local\\\\Application Data.*" wide fullword
        $ = "\\Users\\\\.*?\\\\Start Menu.*" wide fullword
        $ = "\\Users\\\\.*?\\\\Application Data.*" wide fullword
        $ = "\\ProgramData\\\\Microsoft.*" wide fullword
        $ = "\\Users\\\\.*?\\\\AppData\\\\Local\\\\Microsoft.*" wide fullword
        $ = "\\Users\\\\.*?\\\\AppData\\\\Roaming\\\\Microsoft.*" wide fullword
        $ = "\\??\\" wide fullword
        $ = "drivers" wide fullword
        $ = "Microsoft.NET" wide fullword
        $ = "csrss.exe" fullword
        $ = "get_OSVersion" fullword
        $ = "GetDrives" fullword
        $ = "get_IsTerminating" fullword
    condition:
        all of them
}
