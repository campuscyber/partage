rule hermetic_wiper
{
    meta:
        title = "Detection of HermeticWiper samples"
        id = "38ac4ed6-de36-4387-8b16-f8a6a1dae3f4"
        description = "Detection of the destructive HermeticWiper malware involved in Ukraine cyberattacks in February 2022.\nHermeticWiper targets the Master Boot Record and prevents the OS from booting."
        references = "https://www.sentinelone.com/labs/hermetic-wiper-ukraine-under-attack/\nhttps://symantec-enterprise-blogs.security.com/blogs/threat-intelligence/ukraine-wiper-malware-russia"
        date = "2022-02-24"
        modified = "2022-03-10"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002"
        classification = "Windows.Wiper.HermeticWiper"
        os = "Windows"
        score = 100

    strings:
        // Strings for all samples

        $s1 = "\\\\.\\EPMNTDRV\\" wide
        $s2 = "\\\\?\\C:\\Windows\\System32\\winevt\\Logs" fullword wide
        $s3 = "$INDEX_ALLOCATION" fullword wide

        $s4 = "ENIGMA" fullword ascii
        $s5 = "Hermetica Digital Ltd" ascii

        $s6 = "DRV_X64" fullword wide
        $s7 = "DRV_X86" fullword wide
        $s8 = "DRV_XP_X64" fullword wide
        $s9 = "DRV_XP_X86" fullword wide

        // Detection for these samples :
        // c0e0583350f86705a51ca2db9b2d6f77de9129411429eee2bdcfc8aab7f21571
        // 4aa186b5fdcc8248a9672bf21241f77dd395872ec4876c90af5d27ae565e4cb7
        // 0385eeab00e946a302b24a91dea4187c1210597b8e17cd9e2230450f5ece21da
        // 1bc44eef75779e3ca1eefb8ff5a64807dbc942b1e4a2672d77b9f6928d292591
        // 2c10b2ec0b995b88c27d141d6f7b14d6b8177c52818687e4ff8e6ecf53adf5bf
        // 3c557727953a8f6b4788984464fb77741b821991acbf5e746aebdd02615b1767
        // 06086c1da4590dcc7f1e10a6be3431e1166286a9e7761f2de9de79d7fda9c397
        // 291ae59b6edfea6b8555d25714383d1aa343ee23095ba041f197c5bd0cbc0e67
        // ba2888c4eb49268c9594d9837c08affc884172f0e6fc9f988b54a73844bf9152
        // d52113cf2b938447293b195ecfc2b3c9fa61bfab787b6723fc13972b72f90bd5
        // 0d53608c4f7d408e454eafa52b764d8d2bc154d704953c550a29011f759cda2f
        // e259bfd145e3b290f0e205b7177bb6e659e3af236f2aaad8ba57c2d927776018

        // bit of code for the random filename generation
        // based on the current process id.
        $o1 = {
            FF ?? ?? ?? ?? ??  // call    ds:GetCurrentProcessId
            8B F8              // mov     edi, eax
            33 D2              // xor     edx, edx
            6A 04              // push    4               ; cchDestBuffSize
            68 ?? ?? ?? ??     // push    offset pszSrc   ; "drv"
            8D 47 01           // lea     eax, [edi+1]
            F7 F6              // div     esi
            8B CA              // mov     ecx, edx
            33 D2              // xor     edx, edx
            8B C1              // mov     eax, ecx
            F7 F6              // div     esi
            8B F2              // mov     esi, edx
            33 D2              // xor     edx, edx
            8B C6              // mov     eax, esi
            C1 E0 10           // shl     eax, 10h
            03 C1              // add     eax, ecx
            F7 ?? ??           // div     [ebp+var_10]
            0F B7 ?? ?? ??     // movzx   eax, word ptr [ebp+edx*2+alphabet]
            33 D2              // xor     edx, edx
            66 89 03           // mov     [ebx], ax
            8D 04 39           // lea     eax, [ecx+edi]
            B9 F1 FF 00 00     // mov     ecx, 0FFF1h
            F7 F1              // div     ecx
            8B CA              // mov     ecx, edx
            33 D2              // xor     edx, edx
            8D 04 0E           // lea     eax, [esi+ecx]
            BE F1 FF 00 00     // mov     esi, 0FFF1h
            F7 F6              // div     esi
            C1 E2 10           // shl     edx, 10h
            8D 04 11           // lea     eax, [ecx+edx]
            33 D2              // xor     edx, edx
            B9 1A 00 00 00     // mov     ecx, 1Ah
            F7 F1              // div     ecx
            8D 4B 02           // lea     ecx, [ebx+2]
            51                 // push    ecx             ; pszDest
            0F B7 ?? ?? ??     // movzx   eax, word ptr [ebp+edx*2+alphabet]
            66 89 01           // mov     [ecx], ax
            FF ?? ?? ?? ?? ??  // call    ds:StrCatBuffW
            33 C0              // xor     eax, eax
            66 89 43 0C        // mov     [ebx+0Ch], ax
        }

        // Detection for these samples :
        // e7d77ec65309dbff48fe5792defe2e6fafb50f5e5dd95ab03528e6f12c893e3d
        // 1df677af28eb2e393169cf37e3a55a3ab1ef7afdce724d65b8872a7ab87b2640
        // 4351c16a3756328d9ce2ef588e77084b134f6659bf84f4efb5eac80924d636d4
        // c0e0583350f86705a51ca2db9b2d6f77de9129411429eee2bdcfc8aab7f21571

        // Enigma Protector decrypt code
        $o2 = {
            B8 ?? ?? ?? 00                                // mov     eax, 37E0D0h
            03 C5                                         // add     eax, ebp
            81 C0 ?? 00 00 00                             // add     eax, 93h ; '“'
            B9 ?? ?? 00 00                                // mov     ecx, 5B6h
            BA ?? ?? ?? ??                                // mov     edx, 0CBABD56h
                                            // loc_77E154:                             ; CODE XREF: .data:0077E158↓j
            30 10                                         // xor     [eax], dl
            40                                            // inc     eax
            49                                            // dec     ecx
            0F 85 F6 FF FF FF                             // jnz     loc_77E154
            E9 04 00 00 00                                // jmp     loc_77E167
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 2MB and 6 of ($s*) and 1 of ($o*)
}
