rule whispergate_stage1
{
    meta:
        title = "Detection of WhisperGate samples - Stage 1"
        id = "828b5b4c-021c-401c-adc5-15197daf5151"
        description = "Detection of the stage 1 of the destructive malware WhisperGate involved in Ukraine cyberattacks in February 2022.\nWhisperGate corrups the Master Boot Record to display a fake ransom note and then wipes the data."
        references = "https://www.microsoft.com/security/blog/2022/01/15/destructive-malware-targeting-ukrainian-organizations/"
        date = "2022-03-07"
        modified = "2022-03-07"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002"
        classification = "Windows.Wiper.WhisperGate"
        os = "Windows"
        score = 100

    strings:
        // Detection for these samples:
        // b50fb20396458aec55216cc9f5212162b3459bc769a38e050d4d8c22649888ae
        // a196c6b8ffcb97ffb276d04f354696e2391311db3841ae16c8c9f56f36a38e92

        $s1 = "Your hard drive has been corrupted." ascii
        $s2 = "In case you want to recover all hard drives" ascii
        $s3 = "of your organization," ascii
        $s4 = "You should pay us  $10k via bitcoin wallet" ascii
        $s5 = "\\\\.\\PhysicalDrive0" fullword wide

        $mbr = {
            EB 00                       // jmp     short $+3
            8C C8                       // mov     eax, cs
            8E D8                       // mov     ds, eax
            BE 88 7C E8 00              // mov     esi, 0E87C88h
            00 50 FC                    // add     [eax-4], dl
            8A 04 3C                    // mov     al, [esp+edi]
            00 74 06 E8                 // add     [esi+eax-18h], dh
            05 00 46 EB F4              // add     eax, 0F4EB4600h
            EB 05                       // jmp     short loc_404041
            B4 0E                       // mov     ah, 0Eh
            CD 10                       // int     10h
            C3                          // retn
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 1MB and 3 of ($s*) and $mbr
}

rule whispergate_stage2
{
    meta:
        title = "Detection of WhisperGate sample - Stage 2"
        id = "e9d83364-b9e8-460a-9cde-f2441cf47122"
        description = "Detection of the stage 2 of the destructive malware WhisperGate involved in Ukraine cyberattacks in February 2022.\nWhisperGate corrups the Master Boot Record to display a fake ransom note and then wipes the data."
        references = "https://www.microsoft.com/security/blog/2022/01/15/destructive-malware-targeting-ukrainian-organizations/"
        date = "2022-03-07"
        modified = "2022-03-07"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002"
        classification = "Windows.Wiper.WhisperGate"
        os = "Windows"
        score = 100

    strings:
        // Detection for this sample:
        // dcbbae5a1c61dbbbb7dcd6dc5dd1eb1169f5329958d38b58c3fd9384081c9b78

        $s1 = "https://cdn.discordapp.com/attachments/928503440139771947/930108637681184768/Tbopbh.jpg" fullword wide
        $s2 = "DxownxloxadDxatxxax" fullword wide
        $s3 = "<Module>{89a366a7-2270-4665-8440-cb5a27ea74fd}" ascii
        $s4 = "Ylfwdwgmpilzyaph" fullword wide

    condition:
        uint16(0) == 0x5A4D and filesize < 1MB and 3 of ($s*)
}

rule whispergate_stage3
{
    meta:
        title = "Detection of WhisperGate sample - Stage 3"
        id = "95266e0a-a989-4b22-bbae-79635b91e774"
        description = "Detection of the stage 3 of the destructive malware WhisperGate involved in Ukraine cyberattacks in February 2022.\nWhisperGate corrups the Master Boot Record to display a fake ransom note and then wipes the data."
        references = "https://www.microsoft.com/security/blog/2022/01/15/destructive-malware-targeting-ukrainian-organizations/"
        date = "2022-03-07"
        modified = "2022-03-07"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002"
        classification = "Windows.Wiper.WhisperGate"
        os = "Windows"
        score = 100

    strings:
        // Detection for this sample:
        // 9ef7dbd3da51332a78eff19146d21c82957821e464e8133e9594a07d716d892d

        $s1 = "Frkmlkdkdubkznbkmcf" fullword wide
        $s2 = "7c8cb5598e724d34384cce7402b11f0e" fullword wide

        // Hard-coded array of .NET assembly embedded within the DLL.
        $asm = {
            b4 a2 9d 8c 55 f1 b9 30 17 f0
            c0 98 e2 f3 7c c8 09 30 8f 5d
            d5 3a 59 fc 3b f3 3e 29 4f 5e
            ec d9 e6 2f 0d c1 f5 16 0b e1
            5f 2d 29 46 11 16 cd 88 fd 93
            f7 c2 c9 1a e8 65 66 d9 93 fd
            ae 3f 1b 22 72 ba ba a5 77 d3
            ce 49 c8 ec 7c 87 3e 0c aa 05
            df d5 68 24 4b 0e f6 42 a8 c8
            1d d9 13 bb 2f b7 6f 84 34 b4
            e0 11 1d 1b cd 57 5d f2 54 f6
            cd ad 17 f6 16 63 9a 3e af 66
            44 c0 4a 9e e2 e1 3e c2
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 1MB and 2 of ($s*) and $asm
}