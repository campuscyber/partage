rule caddy_wiper
{
    meta:
        title = "Detection of CaddyWiper samples"
        id = "4094e73d-8c7f-48fc-add6-e5ec31fe5fb1"
        description = "Detection of the destructive CaddyWiper malware involved in Ukraine cyberattacks in March 2022.\nCaddyWiper targets user files and other drives and tampers with drive partitions."
        references = "https://www.welivesecurity.com/2022/03/15/caddywiper-new-wiper-malware-discovered-ukraine/"
        date = "2022-03-15"
        modified = "2022-03-15"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.001;attack.t1561.002"
        classification = "Windows.Wiper.CaddyWiper"
        os = "Windows"
        score = 100

    strings:
        // Detection for these samples :
        // 1e87e9b5ee7597bdce796490f3ee09211df48ba1d11f6e2f5b255f05cc0ba176
        // a294620543334a721a2ae8eaaf9680a0786f4b9a216d75b55cfd28f39e9430ea
        // f1e8844dbfc812d39f369e7670545a29efef6764d673038b1c3edd11561d6902
        // ea6a416b320f32261da8dafcf2faf088924f99a3a84f7b43b964637ea87aef72

        // Check if filesize < 10M to prevent huge writes
        $o1 = {
            83 ?? ?? ?? FF FF 00           // cmp     [ebp+fileSize], 0
            73 ??                          // jnb     short loc_4029B8
            E9 ?? ?? 00 00                 // jmp     loc_402A51
            81 ?? ?? ?? FF FF 00 00 A0 00  // cmp     [ebp+fileSize], 0A00000h
            76 ??                          // jbe     short loc_4029CE
            C7 ?? ?? ?? FF FF 00 00 A0 00  // mov     [ebp+fileSize], 0A00000h
        }

        // Call to DeviceIoControl to tamper with partition layout
        $o2 = {
            6A 00              // push    0
            8D ?? ?? ?? FF FF  // lea     eax, [ebp+bytesReturned]
            50                 // push    eax
            6A 00              // push    0
            6A 00              // push    0
            68 80 07 00 00     // push    780h
            8D ?? ?? ?? FF FF  // lea     ecx, [ebp+inBuffer]
            51                 // push    ecx
            68 54 C0 07 00     // push    7C054h --> IOCTL_DISK_SET_DRIVE_LAYOUT_EX
            8B ?? ??           // mov     edx, [ebp+physicalDriverHandle]
            52                 // push    edx
            FF ?? ??           // call    [ebp+DeviceIoControlFunc]
        }

        // Checks for custom file attributes
        $o3 = {
            8B ?? ?? ?? FF FF     // mov     ecx, [ebp+firstFileData.dwFileAttributes]
            83 E1 10              // and     ecx, 10h
            0F ?? ?? 00 00 00     // jz      loc_402913
            0F ?? ?? ?? ?? FF FF  // movsx   edx, [ebp+firstFileData.cFileName]
            83 FA 2E              // cmp     edx, 2Eh ; '.'
            75 ??                 // jnz     short loc_40289E
            0F ?? ?? ?? ?? FF FF  // movsx   eax, [ebp+firstFileData.cFileName+1]
            85 C0                 // test    eax, eax
            74 ??                 // jz      short loc_402899
            0F ?? ?? ?? ?? FF FF  // movsx   ecx, [ebp+firstFileData.cFileName+1]
            83 F9 2E              // cmp     ecx, 2Eh ; '.'
            75 ??                 // jnz     short loc_40289E
            E9 ?? ?? 00 00        // jmp     loc_402A51
            8B ?? ?? ?? FF FF     // mov     edx, [ebp+firstFileData.dwFileAttributes]
            83 E2 02              // and     edx, 2
            75 0B                 // jnz     short loc_4028B4
            8B ?? ?? ?? FF FF     // mov     eax, [ebp+firstFileData.dwFileAttributes]
            83 E0 04              // and     eax, 4
            74 ??                 // jz      short loc_4028B9
            E9 ?? ?? 00 00        // jmp     loc_402A51
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 100KB and all of them
}
