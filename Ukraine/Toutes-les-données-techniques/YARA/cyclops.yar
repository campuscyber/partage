rule CyclopsBlink : malware {
    meta:
        description = "Cyclops Blink, Linux ELF executable"
        source = "OCD"
        date = "16/03/22"
        category = "apt"
    strings:
        $s1 = "-----BEGIN PUBLIC KEY-----\n"
        $s2 = "hw: %02x:%02x:%02x:%02x:%02x:%02x"
        $s3 = "uptime: %lu mem_size: %lu mem_free: %lu"
        $s4 = "disk_size: %lu disk_free: %lu\n"
        $s5 = "%s %s %s %s %s\n"
        $b1 = "Port(1): %d (%d)\n"
        $b2 = "Cmd pid: %d (%d)\n"
        $b3 = "config- <cmd> <arg>\n"
        $b4 = "info-\n"
        $c1 = {50 4F 53 54 20 2F 64 6E 73 2D 71 75 65 72 79 20 48 54 54 50 2F 31 2E 31 0D 0A 48 6F 73 74 3A 20 64 6E 73 2E 67 6F 6F 67 6C 65 0D 0A}
        $c2 = "/var/tmp/a.tmp"
        $c3 = "%s/rootfs_cfg"
        $c4 = "iptables -I %s -p tcp --dport %d -j ACCEPT &>/dev/null"
    condition:
        uint32(0) == 0x464c457f and
        all of ($s*) and
        (all of ($b*) or all of ($c*))
}
