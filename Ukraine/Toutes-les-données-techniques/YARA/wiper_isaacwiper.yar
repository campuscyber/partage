rule isaac_wiper
{
    meta:
        title = "Detection of IsaacWiper samples"
        id = "be96c5da-a49a-42fb-ba9a-75b72ef22be7"
        description = "Detection of the destructive IsaacWiper malware involved in Ukraine cyberattacks in February 2022.\nIsaacWiper targets physical and logical drives, and corrupts them by writing 64kB of random data at offset 0, then corrupts files the same way."
        references = "https://www.welivesecurity.com/2022/03/01/isaacwiper-hermeticwizard-wiper-worm-targeting-ukraine/"
        date = "2022-03-15"
        modified = "2022-03-15"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.001;attack.t1561.002"
        classification = "Windows.Wiper.IsaacWiper"
        os = "Windows"
        score = 100

    strings:
        // Detection for these samples:
        // 13037b749aa4b1eda538fda26d6ac41c8f7b1d02d83f47b0d187dd645154e033
        // 7bcd4ec18fc4a56db30e0aaebd44e2988f98f7b5d8c14f6689f650b4f11e16c0

        $s1 = "C:\\ProgramData\\log.txt" fullword wide
        $s2 = "getting drives..." fullword wide
        $s3 = "physical drives:" fullword wide
        $s4 = "-- system physical drive" fullword wide
        $s5 = "-- physical drive" fullword wide
        $s6 = "logical drives:" fullword wide
        $s7 = "-- system logical drive:" fullword wide
        $s8 = "-- logical drive:" fullword wide
        $s9 = "start erasing physical drives..." fullword wide
        $s10 = "-- FAILED" fullword wide
        $s11 = "physical drive" fullword wide
        $s12 = "-- start erasing logical drive" fullword wide
        $s13 = "start erasing system physical drive..." fullword wide
        $s14 = "system physical drive -- FAILED" fullword wide
        $s15 = "start erasing system logical drive" fullword wide

        // Mersenne twister initialisation
        $o1 = {
            FF ?? ?? ?? ?? ??        // call    ds:GetTickCount
            89 ?? ?? ?? FF FF        // mov     [ebp+var_9F0], eax
            B8 01 00 00 00           // mov     eax, 1
            0F ?? ?? ?? 00 00 00 00  // nop     dword ptr [eax+eax+00000000h]
            8B ?? ?? ?? ?? FF FF     // mov     ecx, [ebp+eax*4+var_9F4]
            8B D1                    // mov     edx, ecx
            C1 EA 1E                 // shr     edx, 1Eh
            33 D1                    // xor     edx, ecx
            69 CA 65 89 07 6C        // imul    ecx, edx, 6C078965h
            03 C8                    // add     ecx, eax
            89 ?? ?? ?? ?? FF FF     // mov     [ebp+eax*4+var_9F0], ecx
            40                       // inc     eax
            3D 70 02 00 00           // cmp     eax, 270h
        }

        // Mersenne twister generation
        $o2 = {
            C1 E8 0B        // shr     eax, 0Bh
            42              // inc     edx
            33 C8           // xor     ecx, eax
            89 ?? ??        // mov     [ebp+var_30], edx
            8B C1           // mov     eax, ecx
            25 AD 58 3A FF  // and     eax, 0FF3A58ADh
            C1 E0 07        // shl     eax, 7
            33 C8           // xor     ecx, eax
            8B C1           // mov     eax, ecx
            25 8C DF FF FF  // and     eax, 0FFFFDF8Ch
            C1 E0 0F        // shl     eax, 0Fh
            33 C8           // xor     ecx, eax
            8B C1           // mov     eax, ecx
            C1 E8 12        // shr     eax, 12h
            33 C1           // xor     eax, ecx
        }

        // Check for specific file attribute
        $o3 = {
            8B ?? ?? ?? FF FF        // mov     eax, [ebp+FindFileData.dwFileAttributes]
            C1 E8 04                 // shr     eax, 4
            F6 D0                    // not     al
            A8 01                    // test    al, 1
            8D ?? ?? ?? FF FF        // lea     eax, [ebp+FindFileData.cFileName]
            0F ?? ?? ?? 00 00        // jz      loc_100038DD
            66 ?? ?? ?? ?? FF FF 00  // cmp     [ebp+FindFileData.cFileName], 0
            74 ??                    // jz      short loc_1000383C
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 400KB and 10 of ($s*) and 2 of ($o*)
}
