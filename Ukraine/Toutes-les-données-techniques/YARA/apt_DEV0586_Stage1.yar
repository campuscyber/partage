rule apt_DEV0586_Stage1 {
    meta:
        id = "cf037769-f2e1-4a1e-8ecf-b4e22dc5b32d"
        version = "1.0"
        malware = "Downloader"
        description = "Downloader used by DEV0586 to download Wiper"
        source = "SEKOIA"
        creation_date = "2022-01-16"
        modification_date = "2022-01-16"
        classification = "TLP:WHITE"
        url = "https://www.microsoft.com/security/blog/2022/01/15/destructive-malware-targeting-ukrainian-organizations/"

    strings:
        $s1 = "powershell" wide fullword
        $s2 = "UwB0AGEAcgB0AC" wide fullword
        $s3 = "DxownxloxadDxatxxax" wide fullword
        $s4 = "_CorExeMain" wide fullword
    
    condition:
        (uint16be(0) == 0x4d5a) and
        filesize < 100KB and
        3 of them and  vt.metadata.new_file
}