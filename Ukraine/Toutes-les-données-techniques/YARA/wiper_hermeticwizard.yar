rule hermetic_wizard
{
    meta:
        title = "Detection of HermeticWizard sample"
        id = "1d1e9c25-c5b0-4ab3-8750-6913a210ee5c"
        description = "Detection of the HermeticWizard worm malware involved in Ukraine cyberattacks in February 2022."
        references = "https://www.welivesecurity.com/2022/03/01/isaacwiper-hermeticwizard-wiper-worm-targeting-ukraine/"
        date = "2022-03-10"
        modified = "2022-03-14"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002;attack.lateral_movement;attack.t1570;attack.t1021.002"
        classification = "Windows.Worm.HermeticWizard"
        os = "Windows"
        score = 100

    strings:
        // Detection for this sample:
        // a259e9b0acf375a8bef8dbc27a8a1996ee02a56889cba07ef58c49185ab033ec

        // Representative string present in the sample
        $s1 = "Wizard.dll" ascii
        $s2 = "Hermetica Digital Ltd" ascii
        $s3 = "\\rundll32.exe" wide
        $s4 = "\" #1" wide
        $s5 = ".ocx" wide

        // paylaod decryption loop (inplace)
        $payload_inplace_decryption_loop = {
            53                // push ebx
            8B D9             // mov ebx, ecx
            56                // push esi
            57                // push edi
            8B 13             // mov edx, dword ptr [ebx]
            8B 43 04          // mov eax, dword ptr [ebx + 4]
            2B C2             // sub eax, edx
            C1 E8 02          // shr eax, 2
            8D 72 FC          // lea esi, [edx - 4]
            8D 78 FF          // lea edi, [eax - 1]
            8D 34 86          // lea esi, [esi + eax*4]
            85 FF             // test edi, edi
            7E 16             // jle function_epiologue
            8B 16             // mov edx, dword ptr [esi]
                        // decryption_loop:
            8D 4E FC          // lea ecx, [esi - 4]
            8B 01             // mov eax, dword ptr [ecx]
            33 C2             // xor eax, edx                    // decrypted_block[i] = encrypted_bloc[i] ^ encrypted_bloc[i - 1]
            8B 11             // mov edx, dword ptr [ecx]
            4F                // dec edi
            89 06             // mov dword ptr [esi], eax
            8D 31             // lea esi, [ecx]
            85 FF             // test edi, edi
            7F EE             // jg decryption_loop
            8B 13             // mov edx, dword ptr [ebx]
                        // function_epiologue:
            81 32 A3 B1 29 4A // xor dword ptr [edx], 0x4a29b1a3 // decrypted_block[0] = encrypted_bloc[0] ^ 0x4A29B1A3
            5F                // pop edi
            5E                // pop esi
            5B                // pop ebx
            C3                // ret
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 1MB and all of ($s*) and $payload_inplace_decryption_loop
}

rule hermetic_wizard_smb_spreader
{
    meta:
        title = "Detection of HermeticWizard romance.dll sample"
        id = "b1ae6529-fbc6-470c-a6e9-86f181cf28a5"
        description = "Detection of the HermeticWizard worm malware involved in Ukraine cyberattacks in February 2022.\nThis payload is responsible of propagating itself via SMB."
        references = "https://www.welivesecurity.com/2022/03/01/isaacwiper-hermeticwizard-wiper-worm-targeting-ukraine/"
        date = "2022-03-14"
        modified = "2022-03-15"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002;attack.lateral_movement;attack.t1570;attack.t1021.002"
        classification = "Windows.Worm.HermeticWizard"
        os = "Windows"
        score = 100

    strings:
        // Detection for this sample:
        // 5a300f72e221a228e3a36a043bef878b570529a7abc15559513ea07ae280bb48

        // Representative string present in the sample
        $s1 = "romance.dll" ascii
        $s2 = "Hermetica Digital Ltd" ascii
        $s3 = "cmd /c start regsvr32 /s /i ..\\" ascii
        $s4 = "c%02X%02X%02X%02X%02X%02X" wide
        $s5 = " & start cmd /c \"ping localhost -n 7 & wevtutil cl System\"" ascii
        $s6 = "Qaz123" wide
        $s7 = "Qwerty123" wide
        $s8 = ".dat" ascii
        $s9 = "{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}" wide
        $s10 = "IPC$" ascii

        $smb_propagate_function = {
            68 ?? ?? ?? ??    // push offset "IPC$"
            FF 76 04          // push dword ptr [esi + 4]        // hostname
            68 ?? ?? ?? ??    // push offset "\\\\%s\\%s"
            57                // push edi                        // buffer
            E8 ?? ?? ?? ??    // call sprintf                    // sprintf(buffer, "\\\\%s\\%s", hostname, "IPC$");
            A1 60 F0 04 10    // mov eax, dword ptr [0x1004f060]
            8B CE             // mov ecx, esi
            89 45 E0          // mov dword ptr [ebp - 0x20], eax
            66 A1 64 F0 04 10 // mov ax, word ptr [0x1004f064]
            66 89 45 E4       // mov word ptr [ebp - 0x1c], ax
            8D 45 E0          // lea eax, [ebp - 0x20]
            89 46 30          // mov dword ptr [esi + 0x30], eax
            89 7E 2C          // mov dword ptr [esi + 0x2c], edi
            E8 ?? ?? ?? ??    // call 0xXXXXXXXX
            6A 12             // push 0x12
            59                // pop ecx
            3B C1             // cmp eax, ecx
            B8 34 02 00 00    // mov eax, 0x234
            57                // push edi
            0F 44 C1          // cmove eax, ecx
            89 45 E8          // mov dword ptr [ebp - 0x18], eax
            E8 ?? ?? ?? ??    // call 0xXXXXXXXX
            83 C4 14          // add esp, 0x14
            33 C0             // xor eax, eax
            89 46 2C          // mov dword ptr [esi + 0x2c], eax
            89 46 30          // mov dword ptr [esi + 0x30], eax
            6A 12             // push 0x12
            5F                // pop edi
            39 7D E8          // cmp dword ptr [ebp - 0x18], edi
            0F 85 ?? ?? ?? ?? // jne 0xXXXXXX
            B2 01             // mov dl, 1
            8B CE             // mov ecx, esi
            E8 ?? ?? ?? ??    // call 0xXXXXXXXX
            3B C7             // cmp eax, edi
            0F 85 ?? ?? ?? ?? // jne 0xXXXXXX
            8B CE             // mov ecx, esi
            E8 ?? ?? ?? ??    // call 0xXXXXXXXX
            8B CE             // mov ecx, esi
            8B F8             // mov edi, eax
            E8 ?? ?? ?? ??    // call 0xXXXXXXXX
            33 C0             // xor eax, eax
            6A 12             // push 0x12
            66 89 46 10       // mov word ptr [esi + 0x10], ax
            58                // pop eax
            3B F8             // cmp edi, eax
            0F 85 ?? ?? ?? ?? // jne 0xXXXXXX
            8D 45 FC          // lea eax, [ebp - 4]
            50                // push eax
            FF 75 F0          // push dword ptr [ebp - 0x10]
            FF 75 F4          // push dword ptr [ebp - 0xc]
            53                // push ebx
            51                // push ecx
            E8 ?? ?? ?? ??    // call 0xXXXXXXXX
            83 C4 14          // add esp, 0x14
            6A 12             // push 0x12
            59                // pop ecx
            3B C1             // cmp eax, ecx
            0F 85 ?? ?? ?? ?? // jne 0xXXXXXX
            8B 45 EC          // mov eax, dword ptr [ebp - 0x14]
            83 C0 5A          // add eax, 0x5a
            50                // push eax
            6A 40             // push 0x40
            FF 15 ?? ?? ?? ?? // call LocalAlloc
            8B F0             // mov esi, eax
            85 F6             // test esi, esi
            74 ??             // je 0xXX
            B9 20 EF 04 10    // mov ecx, 0x1004ef20
            8B D6             // mov edx, esi
            2B D1             // sub edx, ecx

        }

    condition:
        uint16(0) == 0x5A4D and filesize < 500KB and all of ($s*) and $smb_propagate_function
}

rule hermetic_wizard_wmi_spreader
{
    meta:
        title = "Detection of HermeticWizard exec_x32.dll sample"
        id = "ec3e846d-9d19-4c4f-b290-cd0e9051e8ce"
        description = "Detection of the HermeticWizard worm malware involved in Ukraine cyberattacks in February 2022.\nThis payload is responsible of propagating itself via WMI."
        references = "https://www.welivesecurity.com/2022/03/01/isaacwiper-hermeticwizard-wiper-worm-targeting-ukraine/"
        date = "2022-03-16"
        modified = "2022-03-17"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002;attack.lateral_movement;attack.t1570;attack.t1021.006"
        classification = "Windows.Worm.HermeticWizard"
        os = "Windows"
        score = 100

    strings:
        // Detection for this sample:
        // 2d29f9ca1d9089ba0399661bb34ba2fd8aba117f04678cd71856d5894aa7150b

        // Representative string present in the sample
        $s1 = "exec_x32.dll" ascii
        $s2 = "Hermetica Digital Ltd" ascii

        $inlined_string_win32_process = {
            6A 57                // push 0x57
            58                   // pop eax
            6A 69                // push 0x69
            66 89 85 6C FF FF FF // mov word ptr [ebp - 0x94], ax
            58                   // pop eax
            6A 6E                // push 0x6e
            66 89 85 6E FF FF FF // mov word ptr [ebp - 0x92], ax
            58                   // pop eax
            6A 33                // push 0x33
            66 89 85 70 FF FF FF // mov word ptr [ebp - 0x90], ax
            58                   // pop eax
            6A 32                // push 0x32
            66 89 85 72 FF FF FF // mov word ptr [ebp - 0x8e], ax
            58                   // pop eax
            6A 5F                // push 0x5f
            66 89 85 74 FF FF FF // mov word ptr [ebp - 0x8c], ax
            58                   // pop eax
            6A 50                // push 0x50
            66 89 85 76 FF FF FF // mov word ptr [ebp - 0x8a], ax
            58                   // pop eax
            6A 72                // push 0x72
            5A                   // pop edx
            6A 6F                // push 0x6f
            59                   // pop ecx
            6A 63                // push 0x63
            66 89 85 78 FF FF FF // mov word ptr [ebp - 0x88], ax
            58                   // pop eax
            6A 65                // push 0x65
            66 89 85 7E FF FF FF // mov word ptr [ebp - 0x82], ax
            58                   // pop eax
            66 89 45 80          // mov word ptr [ebp - 0x80], ax
            6A 73                // push 0x73
            58                   // pop eax
            66 89 45 82          // mov word ptr [ebp - 0x7e], ax
            66 89 45 84          // mov word ptr [ebp - 0x7c], ax
            33 C0                // xor eax, eax
            66 89 45 86          // mov word ptr [ebp - 0x7a], ax
            8D 85 6C FF FF FF    // lea eax, [ebp - 0x94]
            50                   // push eax
            66 89 95 7A FF FF FF // mov word ptr [ebp - 0x86], dx
            66 89 8D 7C FF FF FF // mov word ptr [ebp - 0x84], cx
            FF 15 ?? ?? ?? ??    // call SysAllocString           // SysAllocString("Win32_Process")
        }

        $inlined_string_create = {
            6A 43             // push 0x43
            58                // pop eax
            6A 72             // push 0x72
            66 89 45 B8       // mov word ptr [ebp - 0x48], ax
            58                // pop eax
            6A 65             // push 0x65
            66 89 45 BA       // mov word ptr [ebp - 0x46], ax
            58                // pop eax
            6A 61             // push 0x61
            59                // pop ecx
            66 89 45 BC       // mov word ptr [ebp - 0x44], ax
            66 89 45 C2       // mov word ptr [ebp - 0x3e], ax
            33 C0             // xor eax, eax
            6A 74             // push 0x74
            66 89 4D BE       // mov word ptr [ebp - 0x42], cx
            59                // pop ecx
            66 89 45 C4       // mov word ptr [ebp - 0x3c], ax
            8D 45 B8          // lea eax, [ebp - 0x48]
            50                // push eax
            66 89 4D C0       // mov word ptr [ebp - 0x40], cx
            FF 15 ?? ?? ?? ?? // call SysAllocString           // SysAllocString("Create")
        }

        // "C:\\Windows\\system32\\cmd.exe /c start C:\\Windows\\system32\\regsvr32.exe /s /i C:\\Windows\\%s.dll"
        $inlined_string_propagation = {
            59                   // pop ecx
            6A 6D                // push 0x6d
            58                   // pop eax
            6A 33                // push 0x33
            66 89 85 F2 FE FF FF // mov word ptr [ebp - 0x10e], ax
            58                   // pop eax
            6A 32                // push 0x32
            66 89 85 F4 FE FF FF // mov word ptr [ebp - 0x10c], ax
            58                   // pop eax
            6A 72                // push 0x72
            66 89 95 D6 FE FF FF // mov word ptr [ebp - 0x12a], dx
            66 89 95 E6 FE FF FF // mov word ptr [ebp - 0x11a], dx
            66 89 95 F8 FE FF FF // mov word ptr [ebp - 0x108], dx
            5A                   // pop edx
            6A 67                // push 0x67
            66 89 85 F6 FE FF FF // mov word ptr [ebp - 0x10a], ax
            58                   // pop eax
            6A 73                // push 0x73
            66 89 85 FE FE FF FF // mov word ptr [ebp - 0x102], ax
            58                   // pop eax
            6A 76                // push 0x76
            66 89 85 00 FF FF FF // mov word ptr [ebp - 0x100], ax
            58                   // pop eax
            66 89 8D F0 FE FF FF // mov word ptr [ebp - 0x110], cx
            66 89 95 FA FE FF FF // mov word ptr [ebp - 0x106], dx
            66 89 8D FC FE FF FF // mov word ptr [ebp - 0x104], cx
            66 89 85 02 FF FF FF // mov word ptr [ebp - 0xfe], ax
            6A 33                // push 0x33
            58                   // pop eax
            6A 32                // push 0x32
            66 89 85 06 FF FF FF // mov word ptr [ebp - 0xfa], ax
            58                   // pop eax
            6A 2E                // push 0x2e
            66 89 85 08 FF FF FF // mov word ptr [ebp - 0xf8], ax
            58                   // pop eax
            6A 78                // push 0x78
            66 89 85 0A FF FF FF // mov word ptr [ebp - 0xf6], ax
            66 89 8D 0C FF FF FF // mov word ptr [ebp - 0xf4], cx
            59                   // pop ecx
            6A 65                // push 0x65
            58                   // pop eax
            6A 20                // push 0x20
            66 89 85 10 FF FF FF // mov word ptr [ebp - 0xf0], ax
            58                   // pop eax
            6A 2F                // push 0x2f
            66 89 95 04 FF FF FF // mov word ptr [ebp - 0xfc], dx
            5A                   // pop edx
            6A 73                // push 0x73
            66 89 85 12 FF FF FF // mov word ptr [ebp - 0xee], ax
            66 89 85 18 FF FF FF // mov word ptr [ebp - 0xe8], ax
            66 89 85 1E FF FF FF // mov word ptr [ebp - 0xe2], ax
            66 89 8D 0E FF FF FF // mov word ptr [ebp - 0xf2], cx
            59                   // pop ecx
            6A 69                // push 0x69
            66 89 95 14 FF FF FF // mov word ptr [ebp - 0xec], dx
            66 89 95 1A FF FF FF // mov word ptr [ebp - 0xe6], dx
            5A                   // pop edx
            6A 43                // push 0x43
            58                   // pop eax
            6A 3A                // push 0x3a
            66 89 85 20 FF FF FF // mov word ptr [ebp - 0xe0], ax
            58                   // pop eax
            6A 5C                // push 0x5c
            66 89 85 22 FF FF FF // mov word ptr [ebp - 0xde], ax
            66 89 95 1C FF FF FF // mov word ptr [ebp - 0xe4], dx
            5A                   // pop edx
            6A 57                // push 0x57
            58                   // pop eax
            6A 69                // push 0x69
            66 89 85 26 FF FF FF // mov word ptr [ebp - 0xda], ax
            58                   // pop eax
            6A 6E                // push 0x6e
            66 89 85 28 FF FF FF // mov word ptr [ebp - 0xd8], ax
            58                   // pop eax
            6A 64                // push 0x64
            66 89 85 2A FF FF FF // mov word ptr [ebp - 0xd6], ax
            58                   // pop eax
            6A 6F                // push 0x6f
            66 89 85 2C FF FF FF // mov word ptr [ebp - 0xd4], ax
            58                   // pop eax
            6A 77                // push 0x77
            66 89 85 2E FF FF FF // mov word ptr [ebp - 0xd2], ax
            58                   // pop eax
            6A 25                // push 0x25
            66 89 85 30 FF FF FF // mov word ptr [ebp - 0xd0], ax
            58                   // pop eax
            6A 2E                // push 0x2e
            66 89 85 36 FF FF FF // mov word ptr [ebp - 0xca], ax
            58                   // pop eax
            6A 64                // push 0x64
            66 89 85 3A FF FF FF // mov word ptr [ebp - 0xc6], ax
            58                   // pop eax
            66 89 85 3C FF FF FF // mov word ptr [ebp - 0xc4], ax
            6A 6C                // push 0x6c
        }

    condition:
        uint16(0) == 0x5A4D and filesize < 200KB and all of ($s*) and 2 of ($inlined_string_*)
}
