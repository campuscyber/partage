rule hermetic_ransom
{
    meta:
        title = "Detection of HermeticRansom sample"
        id = "0f46fcd6-b6f6-4b88-bc75-f12944b6f017"
        description = "Detection of the ransomware HermeticRansom (aka PartyTicket) involved in Ukraine cyberattacks in February 2022.\nHermeticRansom is a ransomware written in Go that is decryptable because it does not properly initialize the encryption key."
        references = "https://www.sentinelone.com/labs/hermetic-wiper-ukraine-under-attack/\nhttps://symantec-enterprise-blogs.security.com/blogs/threat-intelligence/ukraine-wiper-malware-russia"
        date = "2022-03-08"
        modified = "2022-03-08"
        author = "Harfanglab"
        tags = "attack.impact;attack.t1485;attack.t1561.002"
        classification = "Windows.Ransomware.HermeticRansom"
        os = "Windows"
        score = 100

    strings:
        // Detection for this sample:
        // 4dc13bb83a16d4ff9865a51b3e4d24112327c526c1392e14d56f20d6f4eaf382

        $s1 = "403forBiden/wHiteHousE" ascii
        $s2 = "The only thing that we learn from new elections is we learned nothing from the old!" ascii
        $s3 = "Thank you for your vote! All your files, documents, photoes, videos, databases etc. have been successfully encrypted!" ascii
        $s4 = "Now your computer has a special ID:" ascii
        $s5 = "Do not try to decrypt then by yourself - it's impossible!" ascii
        $s6 = "vote2024forjb@protonmail.com" ascii
        $s7 = "encryptedJBadvapi32.dll" ascii

        $go_buildid1 = "Go build ID:" ascii
        $go_buildid2 = "qb0H7AdWAYDzfMA1J80B/nJ9FF8fupJl4qnE4WvA5/PWkwEJfKUrRbYN59_Jba/2o0VIyvqINFbLsDsFyL2" ascii

    condition:
        uint16(0) == 0x5A4D and filesize < 5MB and ((5 of ($s*)) or (all of ($go_buildid*)))
}
